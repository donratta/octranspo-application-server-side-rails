# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130521034322) do

  create_table "access_keys", :force => true do |t|
    t.string   "key"
    t.string   "secret"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "friendtracks", :force => true do |t|
    t.string   "apikey"
    t.float    "latitude1"
    t.float    "longitude1"
    t.float    "latitude2"
    t.float    "longitude"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "houseparties", :force => true do |t|
    t.string   "name"
    t.text     "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "housepartysongs", :force => true do |t|
    t.integer  "houseparty_id"
    t.integer  "song_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "ourplaylists", :force => true do |t|
    t.string   "name"
    t.text     "urlofxml"
    t.text     "description"
    t.text     "imageurl"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.text     "searchparameter"
    t.string   "genre"
    t.integer  "playcount"
  end

  create_table "playlists", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "rating"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "playlistsongs", :force => true do |t|
    t.integer  "playlist_id"
    t.integer  "song_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "songs", :force => true do |t|
    t.string   "title"
    t.string   "artist"
    t.string   "url"
    t.string   "label"
    t.string   "genre"
    t.text     "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "artwork"
  end

  create_table "stops", :id => false, :force => true do |t|
    t.text    "stopid"
    t.integer "stopcode"
    t.text    "stopname"
    t.string  "stopdesc"
    t.float   "stoplat"
    t.float   "stoplon"
    t.string  "stopstreet"
    t.string  "stopcity"
    t.string  "stopregion"
    t.string  "stoppostcode"
    t.string  "stopcountry"
    t.string  "zoneid"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.string   "city"
    t.string   "country"
    t.string   "username"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "picture"
  end

end
