class CreateStops < ActiveRecord::Migration
  def change
    create_table :stops ,:id=>false do |t|
      t.text :stopid
      t.integer :stopcode
      t.text :stopname
      t.string :stopdesc
      t.float :stoplat
      t.float :stoplon
      t.string :stopstreet
      t.string :stopcity
      t.string :stopregion
      t.string :stoppostcode
      t.string :stopcountry
      t.string :zoneid
    end
  end
end
