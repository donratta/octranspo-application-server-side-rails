class CreateFriendtracks < ActiveRecord::Migration
  def change
    create_table :friendtracks do |t|
      t.string :apikey
      t.float :latitude1
      t.float :longitude1
      t.float :latitude2
      t.float :longitude

      t.timestamps
    end
  end
end
