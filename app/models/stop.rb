# == Schema Information
#
# Table name: stops
#
#  stopid       :text
#  stopcode     :string(6)
#  stopname     :text
#  stopdesc     :string(255)
#  stoplat      :float
#  stoplon      :float
#  stopstreet   :string(255)
#  stopcity     :string(255)
#  stopregion   :string(255)
#  stoppostcode :string(255)
#  stopcountry  :string(255)
#  zoneid       :string(255)
#

class Stop < ActiveRecord::Base
   acts_as_mappable :default_units => :kms, 
                   :default_formula => :sphere, 
                   :distance_field_name => :distance,
                   :lat_column_name => :stoplat,
                   :lng_column_name => :stoplon
end
