class StopController < ApplicationController
  def findnearestbusstop
    lat = (params[:lat])
    lat = lat.to_f
    
    lng = (params[:lng])
    lng = lng.to_f
    home = [lat,lng]
    @closestStops= Stop.geo_scope(:origin=>[lat,lng],:within=>0.5)
    
    @result ={:length=>@closestStops.count,:stops=>@closestStops,:callback=>(params[:callback])}    
    #result = result + :params[:callback]
    
    respond_to do |format|
      format.json do        
        render :json => @result, :callback => params[:callback]
      end     
    
    end    
    
    end 
        
    
  end
